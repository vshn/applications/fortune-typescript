import { Application, Router } from "https://deno.land/x/oak@v12.6.0/mod.ts"
import { Handlebars } from "https://deno.land/x/handlebars@v0.10.0/mod.ts"

Deno.addSignalListener("SIGINT", () => {
  Deno.exit()
})

const app = new Application()
const router = new Router()
const hbs = new Handlebars()
const hostname = Deno.hostname()
const version = "1.4-typescript"

// tag::router[]
router.get("/", async (ctx) => {
  const fortune = await shellExec("fortune")
  const random = Math.floor(Math.random() * 1000)
  const data = {
    "message": fortune,
    "hostname": hostname,
    "version": version,
    "number": random,
  }
  const accept = ctx.request.headers.get("accept")
  if (accept === "application/json") {
    ctx.response.body = data
  } else if (accept === "text/plain") {
    ctx.response.body = `Fortune ${version} cookie of the day #${random}:\n\n${fortune}`
  } else {
    ctx.response.body = await hbs.renderView("index", data)
  }
})
// end::router[]

const shellExec = async (path: string): Promise<string> => {
  const command = new Deno.Command(path, { stdout: "piped" })
  const output = await command.output()
  return new TextDecoder().decode(output.stdout)
}

// Start application
app.use(router.routes())
console.log(`Now listening on http://0.0.0.0:8080`)
await app.listen("0.0.0.0:8080")
