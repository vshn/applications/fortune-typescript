FROM docker.io/denoland/deno:alpine-1.35.2 AS build
WORKDIR /app
COPY ["deno.json", "main.ts", "import_map.json", "/app/"]
RUN deno compile main.ts --allow-sys --allow-net --allow-env --allow-read --allow-write --allow-run --output fortune-typescript --target x86_64-unknown-linux-gnu

# tag::production[]
FROM docker.io/frolvlad/alpine-glibc:alpine-3.17
RUN apk update && apk add --no-cache fortune
WORKDIR /app
COPY --from=build /app/fortune-typescript /app/fortune-typescript
COPY views /app/views

EXPOSE 8080

# <1>
USER 1001:0

CMD ["/app/fortune-typescript"]
# end::production[]
